//khai báo thư viện express
const express = require("express");

//khai báo router
const {companyRouter} = require('./app/router/companies');

//khai báo app chạy express
const app = express();

app.use("/", companyRouter);

//khai báo cổng chạy app
const port = 8000;

//khởi tạo chạy app 
app.listen(port,() => {
    console.log(`App listening on port ${port}`);
})